Terms and Conditions

Please read these terms and conditions (&quot;terms and conditions&quot;, &quot;terms&quot;) carefully before using
the Farmers Market mobile app (“app”, &quot;service&quot;) operated by Candescent Limited (&quot;us&quot;, &#39;we&quot;,
&quot;our&quot;).
Conditions of use
By using this app, you certify that you have read and reviewed this Agreement and that you
agree to comply with its terms. If you do not want to be bound by the terms of this Agreement,
you are advised to leave the app accordingly. Candescent Limited only grants use and access
of this app, its products, and its services to those who have accepted its terms.
Privacy policy
Before you continue using our app, we advise you to read our privacy policy regarding our user
data collection. Your data belongs to you. As part of maintenance of the app, Candescent
Limited may access your data—all data submitted will however be treated with the utmost
privacy. Aggregated and anonymized data may be collected and shared with third parties.
Intellectual property
You agree that all materials, products, and services provided on this app are the property of
Candescent Limited, its affiliates, directors, officers, employees, agents, suppliers, or licensors
including all copyrights, trade secrets, trademarks, patents, and other intellectual property. You
also agree that you will not reproduce or redistribute the Candescent Limited’s intellectual
property in any way, including electronic, digital, or new trademark registrations.
You grant Candescent Limited a royalty-free and non-exclusive license to display, use, copy,
transmit, and broadcast the content you upload and publish. For issues regarding intellectual
property claims, you should contact the company in order to come to an agreement.
User accounts
As a user of this app, you may be asked to register with us and provide private information. You
are responsible for ensuring the accuracy of this information, and you are responsible for
maintaining the safety and security of your identifying information. You are also responsible for
all activities that occur under your account or password.
If you think there are any possible issues regarding the security of your account on the app,
inform us immediately so we may address them accordingly.
We reserve all rights to terminate accounts, edit or remove content and cancel orders at our
sole discretion.

Third Party Websites and Content
We allow advertisers to display their advertisements and other information in certain areas of
the app, such as sidebar advertisements or banner advertisements. The advertisements may
contain links to other websites (&quot;Third-Party Websites&quot;) as well as articles, photographs, text,
graphics, pictures, designs, music, sound, video, information, applications, software, and other

Terms and conditions

content or items belonging to or originating from third parties (&quot;Third-Party Content&quot;). Such
Third-Party Websites and Third-Party Content are not investigated, monitored, or checked for
accuracy, appropriateness, or completeness by us, and we are not responsible for any Third-
Party Websites accessed through the Site or any Third-Party Content posted on, available
through, or installed from the Site, including the content, accuracy, offensiveness, opinions,
reliability, privacy practices, or other policies of or contained in the Third-Party Websites or the
Third-Party Content.
Applicable law
By visiting this app, you agree that the laws of your location, without regard to principles of
conflict laws, will govern these terms and conditions, or any dispute of any sort that might come
between Candescent Limited and you, or its business partners and associates.
Disputes
Any dispute related in any way to your visit to this app or to products you purchase from us shall
be arbitrated by state or federal court and you consent to exclusive jurisdiction and venue of
such courts.
Indemnification
You agree to indemnify Candescent Limited and its affiliates and hold Candescent Limited
harmless against legal claims and demands that may arise from your use or misuse of our
services. We reserve the right to select our own legal counsel.
Limitation on liability
Candescent Limited is not liable for any damages that may occur to you as a result of your
misuse of our app.
Candescent Limited reserves the right to edit, modify, and change this Agreement at any time.
We shall let our users know of these changes through electronic mail. This Agreement is an
understanding between Candescent Limited and the user, and this supersedes and replaces all
prior agreements regarding the use of this app.